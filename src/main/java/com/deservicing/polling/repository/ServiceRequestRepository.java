package com.deservicing.polling.repository;

import java.util.List;

import com.deservicing.polling.model.TblServiceRequestMaster;

import io.micronaut.data.annotation.Repository;
import io.micronaut.data.jpa.repository.JpaRepository;
import io.micronaut.data.repository.CrudRepository;

@Repository
public interface ServiceRequestRepository extends JpaRepository<TblServiceRequestMaster, Long> {

	List<TblServiceRequestMaster> findAllByStatusCode(String statusCode);
}
