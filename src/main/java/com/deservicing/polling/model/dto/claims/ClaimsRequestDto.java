package com.deservicing.polling.model.dto.claims;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ClaimsRequestDto {

	private String policyNumber;
	
	private String icifClientNo;
	
	private String benefitTypeCd;
	
	private String benefitySubTypeCd;
	
	private String claimRiderCd;
	
	private String causeOfHospitalizationCd;
	
	@JsonFormat(pattern = "d/MM/yyyy")
	private LocalDate startHospitalizationDate;
	
	@JsonFormat(pattern = "d/MM/yyyy")
	private LocalDate endHospitalizationDate;
	
	private String claimClassification;
	
	private String claimSource;
	
	private String relationshipToInsured;
	
	private String contactPerson;
	
	private String modeOfPayment;
	
	private String emailAddres;
	
	private String contactNo;
	
	private List<RequirementDetailDto> requirementDetails = null;
	
	private List<PolicyDetailDto> policyDetails = null;

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getIcifClientNo() {
		return icifClientNo;
	}

	public void setIcifClientNo(String icifClientNo) {
		this.icifClientNo = icifClientNo;
	}

	public String getBenefitTypeCd() {
		return benefitTypeCd;
	}

	public void setBenefitTypeCd(String benefitTypeCd) {
		this.benefitTypeCd = benefitTypeCd;
	}

	public String getBenefitySubTypeCd() {
		return benefitySubTypeCd;
	}

	public void setBenefitySubTypeCd(String benefitySubTypeCd) {
		this.benefitySubTypeCd = benefitySubTypeCd;
	}

	public String getClaimRiderCd() {
		return claimRiderCd;
	}

	public void setClaimRiderCd(String claimRiderCd) {
		this.claimRiderCd = claimRiderCd;
	}

	public String getCauseOfHospitalizationCd() {
		return causeOfHospitalizationCd;
	}

	public void setCauseOfHospitalizationCd(String causeOfHospitalizationCd) {
		this.causeOfHospitalizationCd = causeOfHospitalizationCd;
	}

	public LocalDate getStartHospitalizationDate() {
		return startHospitalizationDate;
	}

	public void setStartHospitalizationDate(LocalDate startHospitalizationDate) {
		this.startHospitalizationDate = startHospitalizationDate;
	}

	public LocalDate getEndHospitalizationDate() {
		return endHospitalizationDate;
	}

	public void setEndHospitalizationDate(LocalDate endHospitalizationDate) {
		this.endHospitalizationDate = endHospitalizationDate;
	}

	public String getClaimClassification() {
		return claimClassification;
	}

	public void setClaimClassification(String claimClassification) {
		this.claimClassification = claimClassification;
	}

	public String getClaimSource() {
		return claimSource;
	}

	public void setClaimSource(String claimSource) {
		this.claimSource = claimSource;
	}

	public String getRelationshipToInsured() {
		return relationshipToInsured;
	}

	public void setRelationshipToInsured(String relationshipToInsured) {
		this.relationshipToInsured = relationshipToInsured;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getModeOfPayment() {
		return modeOfPayment;
	}

	public void setModeOfPayment(String modeOfPayment) {
		this.modeOfPayment = modeOfPayment;
	}

	public String getEmailAddres() {
		return emailAddres;
	}

	public void setEmailAddres(String emailAddres) {
		this.emailAddres = emailAddres;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public List<RequirementDetailDto> getRequirementDetails() {
		return requirementDetails;
	}

	public void setRequirementDetails(List<RequirementDetailDto> requirementDetails) {
		this.requirementDetails = requirementDetails;
	}

	public List<PolicyDetailDto> getPolicyDetails() {
		return policyDetails;
	}

	public void setPolicyDetails(List<PolicyDetailDto> policyDetails) {
		this.policyDetails = policyDetails;
	}


}
