package com.deservicing.polling.model.dto;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

public class NotificationDto {

	private String code;
	
	private String message;
	
	private String description;
	
	private LocalDateTime timestamp;
	
	private Map<String, Object> metadata = new HashMap<String, Object>();

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public Map<String, Object> getMetadata() {
		return metadata;
	}

	public void setMetadata(Map<String, Object> metadata) {
		this.metadata = metadata;
	}
	
	
}
