package com.deservicing.polling.model.dto.salesforce;

public class PaymentDto {

	private String method;

	private String bankName;

	private String accountFirstName;

	private String accountMiddleName;

	private String accountLastName;

	private String howToBeDelivered;

	private String accountNumber;

	private String prefferedMailingAddress;

	private String preferredAdvisor;

	private String nearestPickup;

	private String pickupBranch;

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAccountFirstName() {
		return accountFirstName;
	}

	public void setAccountFirstName(String accountFirstName) {
		this.accountFirstName = accountFirstName;
	}

	public String getAccountMiddleName() {
		return accountMiddleName;
	}

	public void setAccountMiddleName(String accountMiddleName) {
		this.accountMiddleName = accountMiddleName;
	}

	public String getAccountLastName() {
		return accountLastName;
	}

	public void setAccountLastName(String accountLastName) {
		this.accountLastName = accountLastName;
	}

	public String getHowToBeDelivered() {
		return howToBeDelivered;
	}

	public void setHowToBeDelivered(String howToBeDelivered) {
		this.howToBeDelivered = howToBeDelivered;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getPrefferedMailingAddress() {
		return prefferedMailingAddress;
	}

	public void setPrefferedMailingAddress(String prefferedMailingAddress) {
		this.prefferedMailingAddress = prefferedMailingAddress;
	}

	public String getPreferredAdvisor() {
		return preferredAdvisor;
	}

	public void setPreferredAdvisor(String preferredAdvisor) {
		this.preferredAdvisor = preferredAdvisor;
	}

	public String getNearestPickup() {
		return nearestPickup;
	}

	public void setNearestPickup(String nearestPickup) {
		this.nearestPickup = nearestPickup;
	}

	public String getPickupBranch() {
		return pickupBranch;
	}

	public void setPickupBranch(String pickupBranch) {
		this.pickupBranch = pickupBranch;
	}

}