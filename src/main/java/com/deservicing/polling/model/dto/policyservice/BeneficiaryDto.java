package com.deservicing.polling.model.dto.policyservice;

public class BeneficiaryDto {

	private String beneficiaryName;

	private String beneficiaryRelationshipCd;

	private String beneficiaryRelationship;

	private String beneficiaryDesignationCd;

	private String beneficiaryDesignation;

	private String beneficiaryIrrevocableRevocableCd;

	private String beneficiaryIrrevocableRevocable;

	private String beneficiaryTypeCd;

	private String beneficiaryType;

	private String beneficiaryPercentage;

	private String beneficiaryBirthdate;

	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public String getBeneficiaryRelationshipCd() {
		return beneficiaryRelationshipCd;
	}

	public void setBeneficiaryRelationshipCd(String beneficiaryRelationshipCd) {
		this.beneficiaryRelationshipCd = beneficiaryRelationshipCd;
	}

	public String getBeneficiaryRelationship() {
		return beneficiaryRelationship;
	}

	public void setBeneficiaryRelationship(String beneficiaryRelationship) {
		this.beneficiaryRelationship = beneficiaryRelationship;
	}

	public String getBeneficiaryDesignationCd() {
		return beneficiaryDesignationCd;
	}

	public void setBeneficiaryDesignationCd(String beneficiaryDesignationCd) {
		this.beneficiaryDesignationCd = beneficiaryDesignationCd;
	}

	public String getBeneficiaryDesignation() {
		return beneficiaryDesignation;
	}

	public void setBeneficiaryDesignation(String beneficiaryDesignation) {
		this.beneficiaryDesignation = beneficiaryDesignation;
	}

	public String getBeneficiaryIrrevocableRevocableCd() {
		return beneficiaryIrrevocableRevocableCd;
	}

	public void setBeneficiaryIrrevocableRevocableCd(String beneficiaryIrrevocableRevocableCd) {
		this.beneficiaryIrrevocableRevocableCd = beneficiaryIrrevocableRevocableCd;
	}

	public String getBeneficiaryIrrevocableRevocable() {
		return beneficiaryIrrevocableRevocable;
	}

	public void setBeneficiaryIrrevocableRevocable(String beneficiaryIrrevocableRevocable) {
		this.beneficiaryIrrevocableRevocable = beneficiaryIrrevocableRevocable;
	}

	public String getBeneficiaryTypeCd() {
		return beneficiaryTypeCd;
	}

	public void setBeneficiaryTypeCd(String beneficiaryTypeCd) {
		this.beneficiaryTypeCd = beneficiaryTypeCd;
	}

	public String getBeneficiaryType() {
		return beneficiaryType;
	}

	public void setBeneficiaryType(String beneficiaryType) {
		this.beneficiaryType = beneficiaryType;
	}

	public String getBeneficiaryPercentage() {
		return beneficiaryPercentage;
	}

	public void setBeneficiaryPercentage(String beneficiaryPercentage) {
		this.beneficiaryPercentage = beneficiaryPercentage;
	}

	public String getBeneficiaryBirthdate() {
		return beneficiaryBirthdate;
	}

	public void setBeneficiaryBirthdate(String beneficiaryBirthdate) {
		this.beneficiaryBirthdate = beneficiaryBirthdate;
	}

}