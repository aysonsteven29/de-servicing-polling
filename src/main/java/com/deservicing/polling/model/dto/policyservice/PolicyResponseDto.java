package com.deservicing.polling.model.dto.policyservice;

import java.time.LocalDate;
import java.util.List;
import javax.annotation.Generated;

import com.deservicing.polling.model.dto.CoverageDetailDto;
import com.fasterxml.jackson.annotation.JsonFormat;

@Generated("jsonschema2pojo")
public class PolicyResponseDto {

	private String policyNumber;
	
	private String productName;
	
	private String productCode;
	
	private String insuranceType;
	
	private String issueDate;
	
	private String currency;
	
	private Integer faceAmount;
	
	private String policyDuration;
	
	private String policyStatusCd;
	
	private String policyStatusDesc;
	
	private String billTypeCd;
	
	private String billTypeDesc;
	
	private String billModeCd;
	
	private String billModeDesc;
	
	@JsonFormat(pattern = "d/MM/yyyy")
	private LocalDate premiumDueDate;
	
	private String nfoOptCd;
	
	private Integer prepaidAmount;
	
	private String premOffsetCd;
	
	private String premOffsetDt;
	
	private Integer sundryAmount;
	
	private Integer totalAmountDue;
	
	@JsonFormat(pattern = "d/MM/yyyy")
	private LocalDate fundValueAsOfIssueDate;
	
	private Integer sumAssured;
	
	private Integer lastDeposit;
	
	private String lastDepositDate;
	
	@JsonFormat(pattern = "d/MM/yyyy")
	private LocalDate lapseStrtDate;
	
	private String totalInsuranceCvg;
	
	private Integer premiumAmount;
	
	private Integer modalPremium;
	
	private String insuredId;
	
	private String insuredFirstName;
	
	private String insuredMiddleName;
	
	private String insuredLastName;
	
	private String insuredName;
	
	private String insuredGenderCd;
	
	private String insuredGender;
	
	private String insuredSmokerCd;
	
	private String insuredSmokerDesc;
	
	@JsonFormat(pattern = "d/MM/yyyy")
	private LocalDate insuredBirthdate;
	
	private String insuredOccupation;
	
	private String insuredMaritalStatus;
	
	private String insuredHomePhoneNumber;
	
	private String insuredBusinessPhoneNumber;
	
	private String insuredMobileNumber;
	
	private String insuredEmail;
	
	private String insuredCompleteAddress;
	
	private String beneficiaryInstructions;
	
	@JsonFormat(pattern = "d/MM/yyyy")
	private LocalDate coverageAsOfDate;
	
	private List<CoverageDetailDto> coverageDetailList = null;
	
	private List<BeneficiaryDto> beneficiaryList = null;
	
	private DividendEndowmentInformationDto dividendEndowmentInformation;
	
	private List<PlanPayoutDto> planPayoutList = null;
	
	private String servicingAdvisor;
	
	private Integer loanInterestRate;
	
	@JsonFormat(pattern = "d/MM/yyyy")
	private LocalDate reinstatementDate;

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Integer getFaceAmount() {
		return faceAmount;
	}

	public void setFaceAmount(Integer faceAmount) {
		this.faceAmount = faceAmount;
	}

	public String getPolicyDuration() {
		return policyDuration;
	}

	public void setPolicyDuration(String policyDuration) {
		this.policyDuration = policyDuration;
	}

	public String getPolicyStatusCd() {
		return policyStatusCd;
	}

	public void setPolicyStatusCd(String policyStatusCd) {
		this.policyStatusCd = policyStatusCd;
	}

	public String getPolicyStatusDesc() {
		return policyStatusDesc;
	}

	public void setPolicyStatusDesc(String policyStatusDesc) {
		this.policyStatusDesc = policyStatusDesc;
	}

	public String getBillTypeCd() {
		return billTypeCd;
	}

	public void setBillTypeCd(String billTypeCd) {
		this.billTypeCd = billTypeCd;
	}

	public String getBillTypeDesc() {
		return billTypeDesc;
	}

	public void setBillTypeDesc(String billTypeDesc) {
		this.billTypeDesc = billTypeDesc;
	}

	public String getBillModeCd() {
		return billModeCd;
	}

	public void setBillModeCd(String billModeCd) {
		this.billModeCd = billModeCd;
	}

	public String getBillModeDesc() {
		return billModeDesc;
	}

	public void setBillModeDesc(String billModeDesc) {
		this.billModeDesc = billModeDesc;
	}

	public LocalDate getPremiumDueDate() {
		return premiumDueDate;
	}

	public void setPremiumDueDate(LocalDate premiumDueDate) {
		this.premiumDueDate = premiumDueDate;
	}

	public String getNfoOptCd() {
		return nfoOptCd;
	}

	public void setNfoOptCd(String nfoOptCd) {
		this.nfoOptCd = nfoOptCd;
	}

	public Integer getPrepaidAmount() {
		return prepaidAmount;
	}

	public void setPrepaidAmount(Integer prepaidAmount) {
		this.prepaidAmount = prepaidAmount;
	}

	public String getPremOffsetCd() {
		return premOffsetCd;
	}

	public void setPremOffsetCd(String premOffsetCd) {
		this.premOffsetCd = premOffsetCd;
	}

	public String getPremOffsetDt() {
		return premOffsetDt;
	}

	public void setPremOffsetDt(String premOffsetDt) {
		this.premOffsetDt = premOffsetDt;
	}

	public Integer getSundryAmount() {
		return sundryAmount;
	}

	public void setSundryAmount(Integer sundryAmount) {
		this.sundryAmount = sundryAmount;
	}

	public Integer getTotalAmountDue() {
		return totalAmountDue;
	}

	public void setTotalAmountDue(Integer totalAmountDue) {
		this.totalAmountDue = totalAmountDue;
	}

	public LocalDate getFundValueAsOfIssueDate() {
		return fundValueAsOfIssueDate;
	}

	public void setFundValueAsOfIssueDate(LocalDate fundValueAsOfIssueDate) {
		this.fundValueAsOfIssueDate = fundValueAsOfIssueDate;
	}

	public Integer getSumAssured() {
		return sumAssured;
	}

	public void setSumAssured(Integer sumAssured) {
		this.sumAssured = sumAssured;
	}

	public Integer getLastDeposit() {
		return lastDeposit;
	}

	public void setLastDeposit(Integer lastDeposit) {
		this.lastDeposit = lastDeposit;
	}

	public String getLastDepositDate() {
		return lastDepositDate;
	}

	public void setLastDepositDate(String lastDepositDate) {
		this.lastDepositDate = lastDepositDate;
	}

	public LocalDate getLapseStrtDate() {
		return lapseStrtDate;
	}

	public void setLapseStrtDate(LocalDate lapseStrtDate) {
		this.lapseStrtDate = lapseStrtDate;
	}

	public String getTotalInsuranceCvg() {
		return totalInsuranceCvg;
	}

	public void setTotalInsuranceCvg(String totalInsuranceCvg) {
		this.totalInsuranceCvg = totalInsuranceCvg;
	}

	public Integer getPremiumAmount() {
		return premiumAmount;
	}

	public void setPremiumAmount(Integer premiumAmount) {
		this.premiumAmount = premiumAmount;
	}

	public Integer getModalPremium() {
		return modalPremium;
	}

	public void setModalPremium(Integer modalPremium) {
		this.modalPremium = modalPremium;
	}

	public String getInsuredId() {
		return insuredId;
	}

	public void setInsuredId(String insuredId) {
		this.insuredId = insuredId;
	}

	public String getInsuredFirstName() {
		return insuredFirstName;
	}

	public void setInsuredFirstName(String insuredFirstName) {
		this.insuredFirstName = insuredFirstName;
	}

	public String getInsuredMiddleName() {
		return insuredMiddleName;
	}

	public void setInsuredMiddleName(String insuredMiddleName) {
		this.insuredMiddleName = insuredMiddleName;
	}

	public String getInsuredLastName() {
		return insuredLastName;
	}

	public void setInsuredLastName(String insuredLastName) {
		this.insuredLastName = insuredLastName;
	}

	public String getInsuredName() {
		return insuredName;
	}

	public void setInsuredName(String insuredName) {
		this.insuredName = insuredName;
	}

	public String getInsuredGenderCd() {
		return insuredGenderCd;
	}

	public void setInsuredGenderCd(String insuredGenderCd) {
		this.insuredGenderCd = insuredGenderCd;
	}

	public String getInsuredGender() {
		return insuredGender;
	}

	public void setInsuredGender(String insuredGender) {
		this.insuredGender = insuredGender;
	}

	public String getInsuredSmokerCd() {
		return insuredSmokerCd;
	}

	public void setInsuredSmokerCd(String insuredSmokerCd) {
		this.insuredSmokerCd = insuredSmokerCd;
	}

	public String getInsuredSmokerDesc() {
		return insuredSmokerDesc;
	}

	public void setInsuredSmokerDesc(String insuredSmokerDesc) {
		this.insuredSmokerDesc = insuredSmokerDesc;
	}

	public LocalDate getInsuredBirthdate() {
		return insuredBirthdate;
	}

	public void setInsuredBirthdate(LocalDate insuredBirthdate) {
		this.insuredBirthdate = insuredBirthdate;
	}

	public String getInsuredOccupation() {
		return insuredOccupation;
	}

	public void setInsuredOccupation(String insuredOccupation) {
		this.insuredOccupation = insuredOccupation;
	}

	public String getInsuredMaritalStatus() {
		return insuredMaritalStatus;
	}

	public void setInsuredMaritalStatus(String insuredMaritalStatus) {
		this.insuredMaritalStatus = insuredMaritalStatus;
	}

	public String getInsuredHomePhoneNumber() {
		return insuredHomePhoneNumber;
	}

	public void setInsuredHomePhoneNumber(String insuredHomePhoneNumber) {
		this.insuredHomePhoneNumber = insuredHomePhoneNumber;
	}

	public String getInsuredBusinessPhoneNumber() {
		return insuredBusinessPhoneNumber;
	}

	public void setInsuredBusinessPhoneNumber(String insuredBusinessPhoneNumber) {
		this.insuredBusinessPhoneNumber = insuredBusinessPhoneNumber;
	}

	public String getInsuredMobileNumber() {
		return insuredMobileNumber;
	}

	public void setInsuredMobileNumber(String insuredMobileNumber) {
		this.insuredMobileNumber = insuredMobileNumber;
	}

	public String getInsuredEmail() {
		return insuredEmail;
	}

	public void setInsuredEmail(String insuredEmail) {
		this.insuredEmail = insuredEmail;
	}

	public String getInsuredCompleteAddress() {
		return insuredCompleteAddress;
	}

	public void setInsuredCompleteAddress(String insuredCompleteAddress) {
		this.insuredCompleteAddress = insuredCompleteAddress;
	}

	public String getBeneficiaryInstructions() {
		return beneficiaryInstructions;
	}

	public void setBeneficiaryInstructions(String beneficiaryInstructions) {
		this.beneficiaryInstructions = beneficiaryInstructions;
	}

	public LocalDate getCoverageAsOfDate() {
		return coverageAsOfDate;
	}

	public void setCoverageAsOfDate(LocalDate coverageAsOfDate) {
		this.coverageAsOfDate = coverageAsOfDate;
	}

	public List<CoverageDetailDto> getCoverageDetailList() {
		return coverageDetailList;
	}

	public void setCoverageDetailList(List<CoverageDetailDto> coverageDetailList) {
		this.coverageDetailList = coverageDetailList;
	}

	public List<BeneficiaryDto> getBeneficiaryList() {
		return beneficiaryList;
	}

	public void setBeneficiaryList(List<BeneficiaryDto> beneficiaryList) {
		this.beneficiaryList = beneficiaryList;
	}

	public DividendEndowmentInformationDto getDividendEndowmentInformation() {
		return dividendEndowmentInformation;
	}

	public void setDividendEndowmentInformation(DividendEndowmentInformationDto dividendEndowmentInformation) {
		this.dividendEndowmentInformation = dividendEndowmentInformation;
	}

	public List<PlanPayoutDto> getPlanPayoutList() {
		return planPayoutList;
	}

	public void setPlanPayoutList(List<PlanPayoutDto> planPayoutList) {
		this.planPayoutList = planPayoutList;
	}

	public String getServicingAdvisor() {
		return servicingAdvisor;
	}

	public void setServicingAdvisor(String servicingAdvisor) {
		this.servicingAdvisor = servicingAdvisor;
	}

	public Integer getLoanInterestRate() {
		return loanInterestRate;
	}

	public void setLoanInterestRate(Integer loanInterestRate) {
		this.loanInterestRate = loanInterestRate;
	}

	public LocalDate getReinstatementDate() {
		return reinstatementDate;
	}

	public void setReinstatementDate(LocalDate reinstatementDate) {
		this.reinstatementDate = reinstatementDate;
	}

}