package com.deservicing.polling.model.dto.salesforce;

import java.time.LocalDate;
import java.util.List;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonFormat;

@Generated("jsonschema2pojo")
public class CaseRequestDto {

	private String id;
	
	private String icifClientNo;
	
	private String mediumOfCommunuication;
	
	private String benefitSubType;
	
	private String hospitalizationStartDate;

	@JsonFormat(pattern = "d/MM/yyyy")
	private LocalDate hospitalizationEndDate;
	
	private String filedHIBClaimBefore;
	
	private String confinementExplanation;
	
	private String diagnosis;
	
	private String specificDiagnosis;
	
	private String injuryAccidentCause;
	
	private String specificInjuryAccidentCause;
	
	private String drivingTheVehicle;
	
	private String admittedToICU;

	@JsonFormat(pattern = "d/MM/yyyy")
	private LocalDate icuConfinementDate;
	
	private String icuConfinementDischargeDate;
	
	private String icuType;
	
	private InsuredDto insured;
	
	private HospitalDto hospital;
	
	private PhysicianDto physician;
	
	private List<HistoricalPhysicianDto> historicalPhysician = null;
	
	private List<InitialPhysicianDto> initialPhysician = null;
	
	private SmokingDetailsDto smokingDetails;
	
	private AccidentDto accident;
	
	private PolicyOwnerDto policyOwner;
	
	private PaymentDto payment;
	
	private List<PolicyDto> policies = null;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIcifClientNo() {
		return icifClientNo;
	}

	public void setIcifClientNo(String icifClientNo) {
		this.icifClientNo = icifClientNo;
	}

	public String getMediumOfCommunuication() {
		return mediumOfCommunuication;
	}

	public void setMediumOfCommunuication(String mediumOfCommunuication) {
		this.mediumOfCommunuication = mediumOfCommunuication;
	}

	public String getBenefitSubType() {
		return benefitSubType;
	}

	public void setBenefitSubType(String benefitSubType) {
		this.benefitSubType = benefitSubType;
	}

	public String getHospitalizationStartDate() {
		return hospitalizationStartDate;
	}

	public void setHospitalizationStartDate(String hospitalizationStartDate) {
		this.hospitalizationStartDate = hospitalizationStartDate;
	}

	public LocalDate getHospitalizationEndDate() {
		return hospitalizationEndDate;
	}

	public void setHospitalizationEndDate(LocalDate hospitalizationEndDate) {
		this.hospitalizationEndDate = hospitalizationEndDate;
	}

	public String getFiledHIBClaimBefore() {
		return filedHIBClaimBefore;
	}

	public void setFiledHIBClaimBefore(String filedHIBClaimBefore) {
		this.filedHIBClaimBefore = filedHIBClaimBefore;
	}

	public String getConfinementExplanation() {
		return confinementExplanation;
	}

	public void setConfinementExplanation(String confinementExplanation) {
		this.confinementExplanation = confinementExplanation;
	}

	public String getDiagnosis() {
		return diagnosis;
	}

	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}

	public String getSpecificDiagnosis() {
		return specificDiagnosis;
	}

	public void setSpecificDiagnosis(String specificDiagnosis) {
		this.specificDiagnosis = specificDiagnosis;
	}

	public String getInjuryAccidentCause() {
		return injuryAccidentCause;
	}

	public void setInjuryAccidentCause(String injuryAccidentCause) {
		this.injuryAccidentCause = injuryAccidentCause;
	}

	public String getSpecificInjuryAccidentCause() {
		return specificInjuryAccidentCause;
	}

	public void setSpecificInjuryAccidentCause(String specificInjuryAccidentCause) {
		this.specificInjuryAccidentCause = specificInjuryAccidentCause;
	}

	public String getDrivingTheVehicle() {
		return drivingTheVehicle;
	}

	public void setDrivingTheVehicle(String drivingTheVehicle) {
		this.drivingTheVehicle = drivingTheVehicle;
	}

	public String getAdmittedToICU() {
		return admittedToICU;
	}

	public void setAdmittedToICU(String admittedToICU) {
		this.admittedToICU = admittedToICU;
	}

	public LocalDate getIcuConfinementDate() {
		return icuConfinementDate;
	}

	public void setIcuConfinementDate(LocalDate icuConfinementDate) {
		this.icuConfinementDate = icuConfinementDate;
	}

	public String getIcuConfinementDischargeDate() {
		return icuConfinementDischargeDate;
	}

	public void setIcuConfinementDischargeDate(String icuConfinementDischargeDate) {
		this.icuConfinementDischargeDate = icuConfinementDischargeDate;
	}

	public String getIcuType() {
		return icuType;
	}

	public void setIcuType(String icuType) {
		this.icuType = icuType;
	}

	public InsuredDto getInsured() {
		return insured;
	}

	public void setInsured(InsuredDto insured) {
		this.insured = insured;
	}

	public HospitalDto getHospital() {
		return hospital;
	}

	public void setHospital(HospitalDto hospital) {
		this.hospital = hospital;
	}

	public PhysicianDto getPhysician() {
		return physician;
	}

	public void setPhysician(PhysicianDto physician) {
		this.physician = physician;
	}

	public List<HistoricalPhysicianDto> getHistoricalPhysician() {
		return historicalPhysician;
	}

	public void setHistoricalPhysician(List<HistoricalPhysicianDto> historicalPhysician) {
		this.historicalPhysician = historicalPhysician;
	}

	public List<InitialPhysicianDto> getInitialPhysician() {
		return initialPhysician;
	}

	public void setInitialPhysician(List<InitialPhysicianDto> initialPhysician) {
		this.initialPhysician = initialPhysician;
	}

	public SmokingDetailsDto getSmokingDetails() {
		return smokingDetails;
	}

	public void setSmokingDetails(SmokingDetailsDto smokingDetails) {
		this.smokingDetails = smokingDetails;
	}

	public AccidentDto getAccident() {
		return accident;
	}

	public void setAccident(AccidentDto accident) {
		this.accident = accident;
	}

	public PolicyOwnerDto getPolicyOwner() {
		return policyOwner;
	}

	public void setPolicyOwner(PolicyOwnerDto policyOwner) {
		this.policyOwner = policyOwner;
	}

	public PaymentDto getPayment() {
		return payment;
	}

	public void setPayment(PaymentDto payment) {
		this.payment = payment;
	}

	public List<PolicyDto> getPolicies() {
		return policies;
	}

	public void setPolicies(List<PolicyDto> policies) {
		this.policies = policies;
	}

}