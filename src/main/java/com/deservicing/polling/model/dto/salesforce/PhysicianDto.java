package com.deservicing.polling.model.dto.salesforce;

public class PhysicianDto {

	private String name;

	private String contacNumber;

	private String email;

	private String address;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContacNumber() {
		return contacNumber;
	}

	public void setContacNumber(String contacNumber) {
		this.contacNumber = contacNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}