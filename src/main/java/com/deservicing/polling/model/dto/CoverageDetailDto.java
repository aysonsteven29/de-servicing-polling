package com.deservicing.polling.model.dto;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;

public class CoverageDetailDto {

	@JsonFormat(pattern = "d/MM/yyyy")
	private LocalDate effectiveDate;

	private String policyRiderCode;

	private String embedded;

	private String riderFlag;

	private String policyRiderName;

	private String coverageNum;

	private String coverageStatusCd;

	private String coverageStatus;

	private String coverageFaceAmount;

	@JsonFormat(pattern = "d/MM/yyyy")
	private LocalDate maturityDate;

	public LocalDate getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(LocalDate effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getPolicyRiderCode() {
		return policyRiderCode;
	}

	public void setPolicyRiderCode(String policyRiderCode) {
		this.policyRiderCode = policyRiderCode;
	}

	public String getEmbedded() {
		return embedded;
	}

	public void setEmbedded(String embedded) {
		this.embedded = embedded;
	}

	public String getRiderFlag() {
		return riderFlag;
	}

	public void setRiderFlag(String riderFlag) {
		this.riderFlag = riderFlag;
	}

	public String getPolicyRiderName() {
		return policyRiderName;
	}

	public void setPolicyRiderName(String policyRiderName) {
		this.policyRiderName = policyRiderName;
	}

	public String getCoverageNum() {
		return coverageNum;
	}

	public void setCoverageNum(String coverageNum) {
		this.coverageNum = coverageNum;
	}

	public String getCoverageStatusCd() {
		return coverageStatusCd;
	}

	public void setCoverageStatusCd(String coverageStatusCd) {
		this.coverageStatusCd = coverageStatusCd;
	}

	public String getCoverageStatus() {
		return coverageStatus;
	}

	public void setCoverageStatus(String coverageStatus) {
		this.coverageStatus = coverageStatus;
	}

	public String getCoverageFaceAmount() {
		return coverageFaceAmount;
	}

	public void setCoverageFaceAmount(String coverageFaceAmount) {
		this.coverageFaceAmount = coverageFaceAmount;
	}

	public LocalDate getMaturityDate() {
		return maturityDate;
	}

	public void setMaturityDate(LocalDate maturityDate) {
		this.maturityDate = maturityDate;
	}

}