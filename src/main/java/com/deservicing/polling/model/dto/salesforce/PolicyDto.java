package com.deservicing.polling.model.dto.salesforce;

import java.time.LocalDate;
import java.util.List;
import javax.annotation.Generated;

import com.deservicing.polling.model.dto.CoverageDetailDto;
import com.fasterxml.jackson.annotation.JsonFormat;

@Generated("jsonschema2pojo")
public class PolicyDto {

	private String policyNumber;

	private String policyStatusCd;

	private String productName;

	private String productCode;

	@JsonFormat(pattern = "d/MM/yyyy")
	private LocalDate issueDate;

	private String reinstatementDate;

	private Boolean contestable;

	private String claimRecordNo;

	private String referenceNo;

	private String status;

	private String autoApprove;

	private String message;

	private List<CoverageDetailDto> coverageDetailList = null;

	private List<DocumentDto> documents = null;

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getPolicyStatusCd() {
		return policyStatusCd;
	}

	public void setPolicyStatusCd(String policyStatusCd) {
		this.policyStatusCd = policyStatusCd;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public LocalDate getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(LocalDate issueDate) {
		this.issueDate = issueDate;
	}

	public String getReinstatementDate() {
		return reinstatementDate;
	}

	public void setReinstatementDate(String reinstatementDate) {
		this.reinstatementDate = reinstatementDate;
	}

	public Boolean getContestable() {
		return contestable;
	}

	public void setContestable(Boolean contestable) {
		this.contestable = contestable;
	}

	public String getClaimRecordNo() {
		return claimRecordNo;
	}

	public void setClaimRecordNo(String claimRecordNo) {
		this.claimRecordNo = claimRecordNo;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAutoApprove() {
		return autoApprove;
	}

	public void setAutoApprove(String autoApprove) {
		this.autoApprove = autoApprove;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<CoverageDetailDto> getCoverageDetailList() {
		return coverageDetailList;
	}

	public void setCoverageDetailList(List<CoverageDetailDto> coverageDetailList) {
		this.coverageDetailList = coverageDetailList;
	}

	public List<DocumentDto> getDocuments() {
		return documents;
	}

	public void setDocuments(List<DocumentDto> documents) {
		this.documents = documents;
	}

}