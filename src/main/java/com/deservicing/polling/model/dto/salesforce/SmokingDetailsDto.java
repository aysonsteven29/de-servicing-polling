package com.deservicing.polling.model.dto.salesforce;

import java.time.LocalDate;

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonFormat;

@Generated("jsonschema2pojo")
public class SmokingDetailsDto {

	private Boolean smoking;

	@JsonFormat(pattern = "d/MM/yyyy")
	private LocalDate startDate;

	private String endDate;

	private Boolean stillHabit;

	public Boolean getSmoking() {
		return smoking;
	}

	public void setSmoking(Boolean smoking) {
		this.smoking = smoking;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public Boolean getStillHabit() {
		return stillHabit;
	}

	public void setStillHabit(Boolean stillHabit) {
		this.stillHabit = stillHabit;
	}

}