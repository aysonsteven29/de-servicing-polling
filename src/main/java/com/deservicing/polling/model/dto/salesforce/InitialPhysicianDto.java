package com.deservicing.polling.model.dto.salesforce;

public class InitialPhysicianDto {

	private String name;

	private String contactNumber;

	private String email;

	private String consultationLocation;

	private Boolean otherDoctors;

	private String otherPhysicianName;

	private String otherPhysicianSpecialization;

	private String otherPhysicianAddress;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getConsultationLocation() {
		return consultationLocation;
	}

	public void setConsultationLocation(String consultationLocation) {
		this.consultationLocation = consultationLocation;
	}

	public Boolean getOtherDoctors() {
		return otherDoctors;
	}

	public void setOtherDoctors(Boolean otherDoctors) {
		this.otherDoctors = otherDoctors;
	}

	public String getOtherPhysicianName() {
		return otherPhysicianName;
	}

	public void setOtherPhysicianName(String otherPhysicianName) {
		this.otherPhysicianName = otherPhysicianName;
	}

	public String getOtherPhysicianSpecialization() {
		return otherPhysicianSpecialization;
	}

	public void setOtherPhysicianSpecialization(String otherPhysicianSpecialization) {
		this.otherPhysicianSpecialization = otherPhysicianSpecialization;
	}

	public String getOtherPhysicianAddress() {
		return otherPhysicianAddress;
	}

	public void setOtherPhysicianAddress(String otherPhysicianAddress) {
		this.otherPhysicianAddress = otherPhysicianAddress;
	}

}