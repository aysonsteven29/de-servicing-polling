package com.deservicing.polling.model.dto.claims;

public class RequirementDetailDto {

	private String docType;

	private String dateRecieved;

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public String getDateRecieved() {
		return dateRecieved;
	}

	public void setDateRecieved(String dateRecieved) {
		this.dateRecieved = dateRecieved;
	}

}