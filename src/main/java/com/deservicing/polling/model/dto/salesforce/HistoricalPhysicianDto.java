package com.deservicing.polling.model.dto.salesforce;

public class HistoricalPhysicianDto {

	private Boolean sameCondition;
	
	private String name;
	
	private String consultationDate;
	
	private String treatments;
	
	private Boolean samePhysicianAsFirstPhysician;

	public Boolean getSameCondition() {
		return sameCondition;
	}

	public void setSameCondition(Boolean sameCondition) {
		this.sameCondition = sameCondition;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getConsultationDate() {
		return consultationDate;
	}

	public void setConsultationDate(String consultationDate) {
		this.consultationDate = consultationDate;
	}

	public String getTreatments() {
		return treatments;
	}

	public void setTreatments(String treatments) {
		this.treatments = treatments;
	}

	public Boolean getSamePhysicianAsFirstPhysician() {
		return samePhysicianAsFirstPhysician;
	}

	public void setSamePhysicianAsFirstPhysician(Boolean samePhysicianAsFirstPhysician) {
		this.samePhysicianAsFirstPhysician = samePhysicianAsFirstPhysician;
	}

}