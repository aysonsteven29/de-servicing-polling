package com.deservicing.polling.model.dto.policyservice;

import java.time.LocalDate;

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonFormat;

@Generated("jsonschema2pojo")
public class DividendEndowmentInformationDto {

	private Integer dividendDeclared;
	
	@JsonFormat(pattern = "d/MM/yyyy")
	private LocalDate dividendValueDate;
	
	private String dividendOptCd;
	
	private String dividendOptDesc;
	
	private String dividendYear;
	
	private Integer accumulatedDividend;
	
	private Integer accumulatedEndowmentOption;

	public Integer getDividendDeclared() {
		return dividendDeclared;
	}

	public void setDividendDeclared(Integer dividendDeclared) {
		this.dividendDeclared = dividendDeclared;
	}

	public LocalDate getDividendValueDate() {
		return dividendValueDate;
	}

	public void setDividendValueDate(LocalDate dividendValueDate) {
		this.dividendValueDate = dividendValueDate;
	}

	public String getDividendOptCd() {
		return dividendOptCd;
	}

	public void setDividendOptCd(String dividendOptCd) {
		this.dividendOptCd = dividendOptCd;
	}

	public String getDividendOptDesc() {
		return dividendOptDesc;
	}

	public void setDividendOptDesc(String dividendOptDesc) {
		this.dividendOptDesc = dividendOptDesc;
	}

	public String getDividendYear() {
		return dividendYear;
	}

	public void setDividendYear(String dividendYear) {
		this.dividendYear = dividendYear;
	}

	public Integer getAccumulatedDividend() {
		return accumulatedDividend;
	}

	public void setAccumulatedDividend(Integer accumulatedDividend) {
		this.accumulatedDividend = accumulatedDividend;
	}

	public Integer getAccumulatedEndowmentOption() {
		return accumulatedEndowmentOption;
	}

	public void setAccumulatedEndowmentOption(Integer accumulatedEndowmentOption) {
		this.accumulatedEndowmentOption = accumulatedEndowmentOption;
	}

}