package com.deservicing.polling.model.dto.salesforce;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;

public class AccidentDto {

	@JsonFormat(pattern = "d/MM/yyyy")
	private LocalDate date;
	
	private String time;
	
	private String location;
	
	private String description;

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}