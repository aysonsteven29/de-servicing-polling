package com.deservicing.polling.model.dto.salesforce;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;

public class DocumentDto {

	private String id;
	
	private String typeId;
	
	private String documentTitle;
	
	private String fileName;
	
	private String referenceNo;
	
	@JsonFormat(pattern = "d/MM/yyyy")
	private LocalDate uploadedDate;
	
	private String extension;
	
	private String claimDocType;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public String getDocumentTitle() {
		return documentTitle;
	}

	public void setDocumentTitle(String documentTitle) {
		this.documentTitle = documentTitle;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public LocalDate getUploadedDate() {
		return uploadedDate;
	}

	public void setUploadedDate(LocalDate uploadedDate) {
		this.uploadedDate = uploadedDate;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getClaimDocType() {
		return claimDocType;
	}

	public void setClaimDocType(String claimDocType) {
		this.claimDocType = claimDocType;
	}

}