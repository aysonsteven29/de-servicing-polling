package com.deservicing.polling.model.dto.clientservice;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;

public class AddressChangeRequestDto {

	private String icifClientNo;
	
	private String sourceSystem;
	
	private String firstName;
	
	private String lastName;
	
	private String middleName;
	
	@JsonFormat(pattern = "d/MM/yyyy")
	private LocalDate birthdate;
	
	private String addressType;
	
	private String addressLine1;
	
	private String addressLine2;
	
	private String addressLine3;
	
	private String city;
	
	private String province;
	
	private String country;
	
	private String zipCode;
	
	private Boolean isDefault;
	
	private Boolean isPresentAddress;
	
	private Boolean isLife;
	
	private Boolean isMutualFund;
	
	private Boolean isPreNeed;
	
	private ContactInfoDto contactInfo;
	
	private String countryCode;
	
	private String provinceCode;
	
	private String cityCode;

	public String getIcifClientNo() {
		return icifClientNo;
	}

	public void setIcifClientNo(String icifClientNo) {
		this.icifClientNo = icifClientNo;
	}

	public String getSourceSystem() {
		return sourceSystem;
	}

	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getAddressLine3() {
		return addressLine3;
	}

	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public Boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public Boolean getIsPresentAddress() {
		return isPresentAddress;
	}

	public void setIsPresentAddress(Boolean isPresentAddress) {
		this.isPresentAddress = isPresentAddress;
	}

	public Boolean getIsLife() {
		return isLife;
	}

	public void setIsLife(Boolean isLife) {
		this.isLife = isLife;
	}

	public Boolean getIsMutualFund() {
		return isMutualFund;
	}

	public void setIsMutualFund(Boolean isMutualFund) {
		this.isMutualFund = isMutualFund;
	}

	public Boolean getIsPreNeed() {
		return isPreNeed;
	}

	public void setIsPreNeed(Boolean isPreNeed) {
		this.isPreNeed = isPreNeed;
	}

	public ContactInfoDto getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(ContactInfoDto contactInfo) {
		this.contactInfo = contactInfo;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getProvinceCode() {
		return provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

}