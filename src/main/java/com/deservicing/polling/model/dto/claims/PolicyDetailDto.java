package com.deservicing.polling.model.dto.claims;

import java.time.LocalDate;
import java.util.List;

import com.deservicing.polling.model.dto.CoverageDetailDto;
import com.fasterxml.jackson.annotation.JsonFormat;

public class PolicyDetailDto {

	private String policyNumber;
	
	private String policySuffix;
	
	private String policyOwner;
	
	private String productName;
	
	private String productCode;
	
	private String insuranceType;
	
	private String issueDate;
	
	private String currency;
	
	private String osDisbursementAmount;

	@JsonFormat(pattern = "d/MM/yyyy")
	private LocalDate policyIssueDate;

	@JsonFormat(pattern = "d/MM/yyyy")
	private LocalDate reinstatementDate;
	
	private String policyDuration;
	
	private String policyStatusCd;
	
	private String policyStatusDesc;
	
	private String billTypeCd;
	
	private String billTypeDesc;
	
	private String billModeCd;
	
	private String billModeDesc;

	@JsonFormat(pattern = "d/MM/yyyy")
	private LocalDate premiumDueDate;
	
	private String nfoOptCd;
	
	private String prepaidAmt;
	
	private String premOffsetCd;
	
	private String premOffsetDt;
	
	private String sundryAmount;
	
	private String totalAmountDue;
	
	private String totalFundValue;
	
	private String aeFundAmount;

	@JsonFormat(pattern = "d/MM/yyyy")
	private LocalDate fundValueAsOfIssueDate;
	
	private String sumAssured;
	
	private String lastDeposit;
	
	@JsonFormat(pattern = "d/MM/yyyy")
	private LocalDate lastDepositDate;
	
	private String totalInsuranceCvg;
	
	private String previousPremiumAmount;
	
	private String premiumAmount;
	
	private String modalPremium;
	
	private String miscellaneousSuspenseAmount;
	
	private String policyPdfAmount;
	
	private String dividendOptCd;
	
	private String clientCountryDesc;
	
	private String clientCurrentLocationDesc;
	
	private String ownerClientid;
	
	private String policyComment;
	
	private String paidUpAdditionsCashValue;
	
	private String mailingAddress1;
	
	private String mailingAddress2;
	
	private String mailingAddress3;
	
	private String pvDvLoanAmount;
	
	private String afdDvLoanBal;
	
	private String ownerLastName;
	
	private String ownerFirstName;
	
	private String ownerMiddeName;
	
	private String insuredId;
	
	private String insuredFirstName;
	
	private String insuredMiddleName;
	
	private String insuredLastName;
	
	private String insuredGender;

	@JsonFormat(pattern = "d/MM/yyyy")
	private LocalDate insuredBirthdate;

	private String attainedAge;

	@JsonFormat(pattern = "d/MM/yyyy")
	private LocalDate lapseStartDate;

	@JsonFormat(pattern = "d/MM/yyyy")
	private LocalDate paidToDate;
	
	private String trusteeName;
	
	private String nonForfeitureOption;
	
	private String appAmount;
	
	private String cashSurrenderValue;
	
	private String cashValuePua;
	
	private String maxLoanAmt;
	
	private String loanAmount;
	
	private List<CoverageDetailDto> coverageDetails = null;
	
	private List<BeneficiaryDetailDto> beneficiaryDetails = null;
	
	private String servicingAdvisor;
	
	private String loanInterestRate;
	
	private String agentCode;
	
	private String agentStatus;
	
	private String agentBranch;
	
	private String truePremium;
	
	private String reinsuranceCode;
	
	private String totalDividends;
	
	private String city;
	
	private String postalCode;

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getPolicySuffix() {
		return policySuffix;
	}

	public void setPolicySuffix(String policySuffix) {
		this.policySuffix = policySuffix;
	}

	public String getPolicyOwner() {
		return policyOwner;
	}

	public void setPolicyOwner(String policyOwner) {
		this.policyOwner = policyOwner;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getOsDisbursementAmount() {
		return osDisbursementAmount;
	}

	public void setOsDisbursementAmount(String osDisbursementAmount) {
		this.osDisbursementAmount = osDisbursementAmount;
	}

	public LocalDate getPolicyIssueDate() {
		return policyIssueDate;
	}

	public void setPolicyIssueDate(LocalDate policyIssueDate) {
		this.policyIssueDate = policyIssueDate;
	}

	public LocalDate getReinstatementDate() {
		return reinstatementDate;
	}

	public void setReinstatementDate(LocalDate reinstatementDate) {
		this.reinstatementDate = reinstatementDate;
	}

	public String getPolicyDuration() {
		return policyDuration;
	}

	public void setPolicyDuration(String policyDuration) {
		this.policyDuration = policyDuration;
	}

	public String getPolicyStatusCd() {
		return policyStatusCd;
	}

	public void setPolicyStatusCd(String policyStatusCd) {
		this.policyStatusCd = policyStatusCd;
	}

	public String getPolicyStatusDesc() {
		return policyStatusDesc;
	}

	public void setPolicyStatusDesc(String policyStatusDesc) {
		this.policyStatusDesc = policyStatusDesc;
	}

	public String getBillTypeCd() {
		return billTypeCd;
	}

	public void setBillTypeCd(String billTypeCd) {
		this.billTypeCd = billTypeCd;
	}

	public String getBillTypeDesc() {
		return billTypeDesc;
	}

	public void setBillTypeDesc(String billTypeDesc) {
		this.billTypeDesc = billTypeDesc;
	}

	public String getBillModeCd() {
		return billModeCd;
	}

	public void setBillModeCd(String billModeCd) {
		this.billModeCd = billModeCd;
	}

	public String getBillModeDesc() {
		return billModeDesc;
	}

	public void setBillModeDesc(String billModeDesc) {
		this.billModeDesc = billModeDesc;
	}

	public LocalDate getPremiumDueDate() {
		return premiumDueDate;
	}

	public void setPremiumDueDate(LocalDate premiumDueDate) {
		this.premiumDueDate = premiumDueDate;
	}

	public String getNfoOptCd() {
		return nfoOptCd;
	}

	public void setNfoOptCd(String nfoOptCd) {
		this.nfoOptCd = nfoOptCd;
	}

	public String getPrepaidAmt() {
		return prepaidAmt;
	}

	public void setPrepaidAmt(String prepaidAmt) {
		this.prepaidAmt = prepaidAmt;
	}

	public String getPremOffsetCd() {
		return premOffsetCd;
	}

	public void setPremOffsetCd(String premOffsetCd) {
		this.premOffsetCd = premOffsetCd;
	}

	public String getPremOffsetDt() {
		return premOffsetDt;
	}

	public void setPremOffsetDt(String premOffsetDt) {
		this.premOffsetDt = premOffsetDt;
	}

	public String getSundryAmount() {
		return sundryAmount;
	}

	public void setSundryAmount(String sundryAmount) {
		this.sundryAmount = sundryAmount;
	}

	public String getTotalAmountDue() {
		return totalAmountDue;
	}

	public void setTotalAmountDue(String totalAmountDue) {
		this.totalAmountDue = totalAmountDue;
	}

	public String getTotalFundValue() {
		return totalFundValue;
	}

	public void setTotalFundValue(String totalFundValue) {
		this.totalFundValue = totalFundValue;
	}

	public String getAeFundAmount() {
		return aeFundAmount;
	}

	public void setAeFundAmount(String aeFundAmount) {
		this.aeFundAmount = aeFundAmount;
	}

	public LocalDate getFundValueAsOfIssueDate() {
		return fundValueAsOfIssueDate;
	}

	public void setFundValueAsOfIssueDate(LocalDate fundValueAsOfIssueDate) {
		this.fundValueAsOfIssueDate = fundValueAsOfIssueDate;
	}

	public String getSumAssured() {
		return sumAssured;
	}

	public void setSumAssured(String sumAssured) {
		this.sumAssured = sumAssured;
	}

	public String getLastDeposit() {
		return lastDeposit;
	}

	public void setLastDeposit(String lastDeposit) {
		this.lastDeposit = lastDeposit;
	}

	public LocalDate getLastDepositDate() {
		return lastDepositDate;
	}

	public void setLastDepositDate(LocalDate lastDepositDate) {
		this.lastDepositDate = lastDepositDate;
	}

	public String getTotalInsuranceCvg() {
		return totalInsuranceCvg;
	}

	public void setTotalInsuranceCvg(String totalInsuranceCvg) {
		this.totalInsuranceCvg = totalInsuranceCvg;
	}

	public String getPreviousPremiumAmount() {
		return previousPremiumAmount;
	}

	public void setPreviousPremiumAmount(String previousPremiumAmount) {
		this.previousPremiumAmount = previousPremiumAmount;
	}

	public String getPremiumAmount() {
		return premiumAmount;
	}

	public void setPremiumAmount(String premiumAmount) {
		this.premiumAmount = premiumAmount;
	}

	public String getModalPremium() {
		return modalPremium;
	}

	public void setModalPremium(String modalPremium) {
		this.modalPremium = modalPremium;
	}

	public String getMiscellaneousSuspenseAmount() {
		return miscellaneousSuspenseAmount;
	}

	public void setMiscellaneousSuspenseAmount(String miscellaneousSuspenseAmount) {
		this.miscellaneousSuspenseAmount = miscellaneousSuspenseAmount;
	}

	public String getPolicyPdfAmount() {
		return policyPdfAmount;
	}

	public void setPolicyPdfAmount(String policyPdfAmount) {
		this.policyPdfAmount = policyPdfAmount;
	}

	public String getDividendOptCd() {
		return dividendOptCd;
	}

	public void setDividendOptCd(String dividendOptCd) {
		this.dividendOptCd = dividendOptCd;
	}

	public String getClientCountryDesc() {
		return clientCountryDesc;
	}

	public void setClientCountryDesc(String clientCountryDesc) {
		this.clientCountryDesc = clientCountryDesc;
	}

	public String getClientCurrentLocationDesc() {
		return clientCurrentLocationDesc;
	}

	public void setClientCurrentLocationDesc(String clientCurrentLocationDesc) {
		this.clientCurrentLocationDesc = clientCurrentLocationDesc;
	}

	public String getOwnerClientid() {
		return ownerClientid;
	}

	public void setOwnerClientid(String ownerClientid) {
		this.ownerClientid = ownerClientid;
	}

	public String getPolicyComment() {
		return policyComment;
	}

	public void setPolicyComment(String policyComment) {
		this.policyComment = policyComment;
	}

	public String getPaidUpAdditionsCashValue() {
		return paidUpAdditionsCashValue;
	}

	public void setPaidUpAdditionsCashValue(String paidUpAdditionsCashValue) {
		this.paidUpAdditionsCashValue = paidUpAdditionsCashValue;
	}

	public String getMailingAddress1() {
		return mailingAddress1;
	}

	public void setMailingAddress1(String mailingAddress1) {
		this.mailingAddress1 = mailingAddress1;
	}

	public String getMailingAddress2() {
		return mailingAddress2;
	}

	public void setMailingAddress2(String mailingAddress2) {
		this.mailingAddress2 = mailingAddress2;
	}

	public String getMailingAddress3() {
		return mailingAddress3;
	}

	public void setMailingAddress3(String mailingAddress3) {
		this.mailingAddress3 = mailingAddress3;
	}

	public String getPvDvLoanAmount() {
		return pvDvLoanAmount;
	}

	public void setPvDvLoanAmount(String pvDvLoanAmount) {
		this.pvDvLoanAmount = pvDvLoanAmount;
	}

	public String getAfdDvLoanBal() {
		return afdDvLoanBal;
	}

	public void setAfdDvLoanBal(String afdDvLoanBal) {
		this.afdDvLoanBal = afdDvLoanBal;
	}

	public String getOwnerLastName() {
		return ownerLastName;
	}

	public void setOwnerLastName(String ownerLastName) {
		this.ownerLastName = ownerLastName;
	}

	public String getOwnerFirstName() {
		return ownerFirstName;
	}

	public void setOwnerFirstName(String ownerFirstName) {
		this.ownerFirstName = ownerFirstName;
	}

	public String getOwnerMiddeName() {
		return ownerMiddeName;
	}

	public void setOwnerMiddeName(String ownerMiddeName) {
		this.ownerMiddeName = ownerMiddeName;
	}

	public String getInsuredId() {
		return insuredId;
	}

	public void setInsuredId(String insuredId) {
		this.insuredId = insuredId;
	}

	public String getInsuredFirstName() {
		return insuredFirstName;
	}

	public void setInsuredFirstName(String insuredFirstName) {
		this.insuredFirstName = insuredFirstName;
	}

	public String getInsuredMiddleName() {
		return insuredMiddleName;
	}

	public void setInsuredMiddleName(String insuredMiddleName) {
		this.insuredMiddleName = insuredMiddleName;
	}

	public String getInsuredLastName() {
		return insuredLastName;
	}

	public void setInsuredLastName(String insuredLastName) {
		this.insuredLastName = insuredLastName;
	}

	public String getInsuredGender() {
		return insuredGender;
	}

	public void setInsuredGender(String insuredGender) {
		this.insuredGender = insuredGender;
	}

	public LocalDate getInsuredBirthdate() {
		return insuredBirthdate;
	}

	public void setInsuredBirthdate(LocalDate insuredBirthdate) {
		this.insuredBirthdate = insuredBirthdate;
	}

	public String getAttainedAge() {
		return attainedAge;
	}

	public void setAttainedAge(String attainedAge) {
		this.attainedAge = attainedAge;
	}

	public LocalDate getLapseStartDate() {
		return lapseStartDate;
	}

	public void setLapseStartDate(LocalDate lapseStartDate) {
		this.lapseStartDate = lapseStartDate;
	}

	public LocalDate getPaidToDate() {
		return paidToDate;
	}

	public void setPaidToDate(LocalDate paidToDate) {
		this.paidToDate = paidToDate;
	}

	public String getTrusteeName() {
		return trusteeName;
	}

	public void setTrusteeName(String trusteeName) {
		this.trusteeName = trusteeName;
	}

	public String getNonForfeitureOption() {
		return nonForfeitureOption;
	}

	public void setNonForfeitureOption(String nonForfeitureOption) {
		this.nonForfeitureOption = nonForfeitureOption;
	}

	public String getAppAmount() {
		return appAmount;
	}

	public void setAppAmount(String appAmount) {
		this.appAmount = appAmount;
	}

	public String getCashSurrenderValue() {
		return cashSurrenderValue;
	}

	public void setCashSurrenderValue(String cashSurrenderValue) {
		this.cashSurrenderValue = cashSurrenderValue;
	}

	public String getCashValuePua() {
		return cashValuePua;
	}

	public void setCashValuePua(String cashValuePua) {
		this.cashValuePua = cashValuePua;
	}

	public String getMaxLoanAmt() {
		return maxLoanAmt;
	}

	public void setMaxLoanAmt(String maxLoanAmt) {
		this.maxLoanAmt = maxLoanAmt;
	}

	public String getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(String loanAmount) {
		this.loanAmount = loanAmount;
	}

	public List<CoverageDetailDto> getCoverageDetails() {
		return coverageDetails;
	}

	public void setCoverageDetails(List<CoverageDetailDto> coverageDetails) {
		this.coverageDetails = coverageDetails;
	}

	public List<BeneficiaryDetailDto> getBeneficiaryDetails() {
		return beneficiaryDetails;
	}

	public void setBeneficiaryDetails(List<BeneficiaryDetailDto> beneficiaryDetails) {
		this.beneficiaryDetails = beneficiaryDetails;
	}

	public String getServicingAdvisor() {
		return servicingAdvisor;
	}

	public void setServicingAdvisor(String servicingAdvisor) {
		this.servicingAdvisor = servicingAdvisor;
	}

	public String getLoanInterestRate() {
		return loanInterestRate;
	}

	public void setLoanInterestRate(String loanInterestRate) {
		this.loanInterestRate = loanInterestRate;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAgentStatus() {
		return agentStatus;
	}

	public void setAgentStatus(String agentStatus) {
		this.agentStatus = agentStatus;
	}

	public String getAgentBranch() {
		return agentBranch;
	}

	public void setAgentBranch(String agentBranch) {
		this.agentBranch = agentBranch;
	}

	public String getTruePremium() {
		return truePremium;
	}

	public void setTruePremium(String truePremium) {
		this.truePremium = truePremium;
	}

	public String getReinsuranceCode() {
		return reinsuranceCode;
	}

	public void setReinsuranceCode(String reinsuranceCode) {
		this.reinsuranceCode = reinsuranceCode;
	}

	public String getTotalDividends() {
		return totalDividends;
	}

	public void setTotalDividends(String totalDividends) {
		this.totalDividends = totalDividends;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}


}