package com.deservicing.polling.model.dto;

import java.util.ArrayList;
import java.util.List;

public class GenericResponse<T> {

	private T data;
	
	private List<NotificationDto> notifications = new ArrayList<NotificationDto>();

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public List<NotificationDto> getNotifications() {
		return notifications;
	}

	public void setNotifications(List<NotificationDto> notifications) {
		this.notifications = notifications;
	}
	
	
	
}
