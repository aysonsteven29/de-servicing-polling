package com.deservicing.polling.model.dto.policyservice;

import javax.annotation.Generated;

@Generated("jsonschema2pojo")
public class PlanPayoutDto {

	private String planPayoutCd;

	public String getPlanPayoutCd() {
		return planPayoutCd;
	}

	public void setPlanPayoutCd(String planPayoutCd) {
		this.planPayoutCd = planPayoutCd;
	}

}