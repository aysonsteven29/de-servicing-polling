package com.deservicing.polling.model.dto.claims;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;

public class BeneficiaryDetailDto {

	private String beneficiaryName;
	
	private String beneficiaryRelationship;
	
	private String beneficiaryDesignation;
	
	private String beneficiaryIClassification;
	
	private String beneficiaryType;
	
	private String beneficiaryPercentage;
	
	@JsonFormat(pattern = "d/MM/yyyy")
	private LocalDate beneficiaryBirthdate;
	
	private String beneficiaryInstruction;

	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public String getBeneficiaryRelationship() {
		return beneficiaryRelationship;
	}

	public void setBeneficiaryRelationship(String beneficiaryRelationship) {
		this.beneficiaryRelationship = beneficiaryRelationship;
	}

	public String getBeneficiaryDesignation() {
		return beneficiaryDesignation;
	}

	public void setBeneficiaryDesignation(String beneficiaryDesignation) {
		this.beneficiaryDesignation = beneficiaryDesignation;
	}

	public String getBeneficiaryIClassification() {
		return beneficiaryIClassification;
	}

	public void setBeneficiaryIClassification(String beneficiaryIClassification) {
		this.beneficiaryIClassification = beneficiaryIClassification;
	}

	public String getBeneficiaryType() {
		return beneficiaryType;
	}

	public void setBeneficiaryType(String beneficiaryType) {
		this.beneficiaryType = beneficiaryType;
	}

	public String getBeneficiaryPercentage() {
		return beneficiaryPercentage;
	}

	public void setBeneficiaryPercentage(String beneficiaryPercentage) {
		this.beneficiaryPercentage = beneficiaryPercentage;
	}

	public LocalDate getBeneficiaryBirthdate() {
		return beneficiaryBirthdate;
	}

	public void setBeneficiaryBirthdate(LocalDate beneficiaryBirthdate) {
		this.beneficiaryBirthdate = beneficiaryBirthdate;
	}

	public String getBeneficiaryInstruction() {
		return beneficiaryInstruction;
	}

	public void setBeneficiaryInstruction(String beneficiaryInstruction) {
		this.beneficiaryInstruction = beneficiaryInstruction;
	}


}