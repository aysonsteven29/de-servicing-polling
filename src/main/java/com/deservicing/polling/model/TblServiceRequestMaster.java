package com.deservicing.polling.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.deservicing.polling.model.enums.ServiceTypeEnum;
import com.deservicing.polling.model.enums.TouchpointEnum;


@Entity
@Table(name = "service_request_master")
public class TblServiceRequestMaster {
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(name = "tracking_number")
    private String trackingNumber;
    
    @Column(name = "status_code")
    private String statusCode;
    
    @Column(name = "service_type")
    private ServiceTypeEnum serviceType;
    
    @Column(name = "request_type")
    private final String requestType = "SAVE";
    
    @Column
    private TouchpointEnum touchpoint;
    
    @Column(name = "admin_sys_no")
    private String adminSysNo;
    
    @Column(name = "reference_number")
    private Integer referenceNumber;
    
    @Column(name = "transaction_date_time")
    private LocalDate transactionDateTime;
    
    @Column
    private String payload;
    
    @Column
    private String response;
    
    @Column
    private String remarks;
    
    @Column(name = "company_cd")
    private String companyCode;
    
    @Column(name = "created_dt")
    private LocalDate createdDt;
    
    @Column(name = "created_by")
    private String createdBy;
    
    @Column(name = "updated_dt")
    private LocalDate updatedDt;
    
    @Column(name = "updated_by")
    private String updatedBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTrackingNumber() {
		return trackingNumber;
	}

	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public ServiceTypeEnum getServiceType() {
		return serviceType;
	}

	public void setServiceType(ServiceTypeEnum serviceType) {
		this.serviceType = serviceType;
	}

	public TouchpointEnum getTouchpoint() {
		return touchpoint;
	}

	public void setTouchpoint(TouchpointEnum touchpoint) {
		this.touchpoint = touchpoint;
	}

	public String getAdminSysNo() {
		return adminSysNo;
	}

	public void setAdminSysNo(String adminSysNo) {
		this.adminSysNo = adminSysNo;
	}

	public Integer getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(Integer referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public LocalDate getTransactionDateTime() {
		return transactionDateTime;
	}

	public void setTransactionDateTime(LocalDate transactionDateTime) {
		this.transactionDateTime = transactionDateTime;
	}

	public String getPayload() {
		return payload;
	}

	public void setPayload(String payload) {
		this.payload = payload;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getCompany() {
		return companyCode;
	}

	public void setCompany(String company) {
		this.companyCode = company;
	}

	public LocalDate getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(LocalDate createdDt) {
		this.createdDt = createdDt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public LocalDate getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(LocalDate updatedDt) {
		this.updatedDt = updatedDt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getRequestType() {
		return requestType;
	}
    
    
    
    
}
