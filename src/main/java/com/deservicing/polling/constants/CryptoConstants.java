package com.deservicing.polling.constants;

public class CryptoConstants {
    /** HmacSHA256 */
    public static final String HMAC_SHA256_ALGORITHM = "HmacSHA256";  
    /** MD5 */
    public static final String MD5_ALGORITHM = "MD5";
    /** AES */
    public static final String AES_ALGORITHM = "AES";
    /** 256 */
    public static final int ENCRYPTION_BITS_256 = 256;
}
