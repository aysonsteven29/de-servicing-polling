package com.deservicing.polling.exception;

public class CryptoException extends RuntimeException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 * @param message
	 */
	public CryptoException(String message) {
        super(message);
    }

	/**
	 * 
	 * @param message
	 * @param cause
	 */
    public CryptoException(String message, Throwable cause) {
        super(message, cause);
    }
}
