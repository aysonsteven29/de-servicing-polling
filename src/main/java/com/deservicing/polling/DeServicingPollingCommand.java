package com.deservicing.polling;

import java.util.List;

import com.deservicing.polling.httpclient.ClaimsHttpClient;
import com.deservicing.polling.model.TblServiceRequestMaster;
import com.deservicing.polling.repository.ServiceRequestRepository;
import com.deservicing.polling.utils.EncryptionUtil;

import io.micronaut.configuration.picocli.PicocliRunner;
import jakarta.inject.Inject;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

@Command(name = "de-servicing-polling", description = "...",
        mixinStandardHelpOptions = true)
public class DeServicingPollingCommand implements Runnable {

    @Option(names = {"-v", "--verbose"}, description = "...")
    boolean verbose;
    
    @Inject
    private ServiceRequestRepository repository;
    
    @Inject
    private ClaimsHttpClient client;

    public static void main(String[] args) throws Exception {
        PicocliRunner.run(DeServicingPollingCommand.class, args);
        
    }

    public void run() {
    	
    	System.out.println("GRADLE");
    	
    	List<TblServiceRequestMaster> srList= repository.findAllByStatusCode("FLD");
    	
    	
    	
    	srList.forEach(sr->System.out.println(sr));
    	
    	
    	
    	String key = EncryptionUtil.createSecretKey();
    	System.out.println("secret Key: " + key);
    	System.out.println("Value: "+ "Mathew Weissnat");
    	System.out.println("encrypted: " + EncryptionUtil.encrypt("Mathew Weissnat", key));
    }
}
