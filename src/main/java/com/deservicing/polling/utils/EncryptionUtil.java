package com.deservicing.polling.utils;


import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.Charsets;
import org.apache.commons.codec.binary.Base64;

import com.deservicing.polling.constants.CryptoConstants;
import com.deservicing.polling.exception.CryptoException;

public class EncryptionUtil {

	/**
	 * 
	 * @param data as string
	 * @param key
	 * @return
	 */
	public static String encrypt(String data, String key) {	
		return new String(encrypt(data.getBytes(Charsets.UTF_8), key, true), Charsets.UTF_8).trim();
	}
	
	/**
	 * 
	 * @param data as bytes
	 * @param key
	 * @return
	 */
	public static byte[] encrypt(byte[] data, String key) {
		return encrypt(data, key, false);
	}
		
	/**
	 * 
	 * @param data
	 * @param key
	 * @return
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 */
	private static byte[] encrypt(byte[] data, String key, boolean isEncodeBase64) {
	    // Encrypt data using DEK
		byte[] encryptedValue = null;

		try {
			byte[] decodedKey = Base64.decodeBase64(key);
	        Key keyObj = new SecretKeySpec(decodedKey, 0, decodedKey.length, CryptoConstants.AES_ALGORITHM); 
	        
		    Cipher cipher = Cipher.getInstance(CryptoConstants.AES_ALGORITHM);
		    cipher.init(Cipher.ENCRYPT_MODE, keyObj);
		    byte[] encVal = cipher.doFinal(data);
		    if (isEncodeBase64) {
		    	encryptedValue = Base64.encodeBase64(encVal);
		    } else {
		    	encryptedValue = encVal;
		    }
		} catch (Exception e) {
			throw new CryptoException("Unexpected error while encrypting data: " + e.getMessage(), e);
		}
			
		return encryptedValue;
	}
	
	/**
	 * 
	 * @param data as string
	 * @param key
	 * @return
	 */
	public static String decrypt(String data, String key) {
		return new String(decrypt(data.getBytes(Charsets.UTF_8), key, true), Charsets.UTF_8);
	}
	
	/**
	 * 
	 * @param data as bytes
	 * @param key
	 * @return
	 */
	public static byte[] decrypt(byte[] data, String key) {
		return decrypt(data, key, false);
	}
		
	/**
	 * 
	 * @param data
	 * @param key
	 * @return
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 */
	private static byte[] decrypt(byte[] data, String key, boolean isDecodeBase64) {
        // Decrypt data using DEK
		byte[] decryptedValue = null;
		byte[] decodedValue = null;

		try {
			byte[] decodedKey = Base64.decodeBase64(key);
	        Key keyObj = new SecretKeySpec(decodedKey, 0, decodedKey.length, CryptoConstants.AES_ALGORITHM); 
			Cipher cipher = Cipher.getInstance(CryptoConstants.AES_ALGORITHM);
	        cipher.init(Cipher.DECRYPT_MODE, keyObj);
	        
	        if (isDecodeBase64) {
	        	decodedValue = Base64.decodeBase64(data);
	        } else {
	        	decodedValue = data;
	        }
	        
	        decryptedValue = cipher.doFinal(decodedValue);
		} catch (Exception e) {
			throw new CryptoException("Unexpected error while decrypting data: " + e.getMessage(), e);
		}
		
		return decryptedValue;
	}
	
	/**
	 * 
	 * @return
	 */
	public static String createSecretKey() {
		
		String secreKey = "";
		
		try {
			KeyGenerator keyGenerator;
			keyGenerator = KeyGenerator.getInstance(CryptoConstants.AES_ALGORITHM);
		    keyGenerator.init(CryptoConstants.ENCRYPTION_BITS_256, new SecureRandom());
		    SecretKey key = keyGenerator.generateKey();
		    secreKey = Base64.encodeBase64String(key.getEncoded()).trim();    
		} catch (NoSuchAlgorithmException e) {
			throw new CryptoException("Unexpected error while creating key: " + e.getMessage(), e);
		}
			
		return secreKey;
	}
	
	/**
	 * 
	 * @param data
	 * @param secret
	 * @return
	 */
	public static String calculateHmac(String data, String secret) {
		return calculateHmac(data.getBytes(Charsets.UTF_8), secret);
	}
	
	/**
	 * 
	 * @param data
	 * @param secret
	 * @return
	 */
	public static String calculateHmac(byte[] data, String secret) {
		try {
			SecretKeySpec signingKey = new SecretKeySpec(Base64.decodeBase64(secret), 
					CryptoConstants.HMAC_SHA256_ALGORITHM);
			Mac mac = Mac.getInstance(CryptoConstants.HMAC_SHA256_ALGORITHM);
			mac.init(signingKey);
			byte[] rawHmac = mac.doFinal(data);
			String result = new String(Base64.encodeBase64(rawHmac));
			return result;		
		} catch (GeneralSecurityException e) {
			throw new CryptoException("Unexpected error while calculating HMAC: " + e.getMessage(), e);
		}
	}
	
	/**
	 * 
	 * @param data
	 * @return
	 */
	public static String calculateMd5(String data) {
		return calculateMd5(data.getBytes(Charsets.UTF_8));
	}
	
	/**
	 * 
	 * @return
	 */
	public static String calculateMd5(byte[] data) {
		
		MessageDigest digest = null;
		
		try {
			digest = MessageDigest.getInstance(CryptoConstants.MD5_ALGORITHM);
		} catch (NoSuchAlgorithmException e) {
			throw new CryptoException("Unexpected error while calcularing MD5: " + e.getMessage(), e);
		}

		digest.update(data);
			
		String result = new String(Base64.encodeBase64(digest.digest()));
		
		return result;
	}	
	
}