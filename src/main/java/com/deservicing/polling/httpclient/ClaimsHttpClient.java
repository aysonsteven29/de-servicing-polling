package com.deservicing.polling.httpclient;

import com.deservicing.polling.model.dto.GenericResponse;
import com.deservicing.polling.model.dto.claims.ClaimsRequestDto;

import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Header;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.client.annotation.Client;

@Client("${httpclient.claimsAPI}")
public interface ClaimsHttpClient {
	
	@Post("/claims")
	GenericResponse<String> postClaims(
			@Body ClaimsRequestDto requestBody,
			@Header("x-traceability-id") String xTraceAbilityId,
			@Header("x-correlation-id") String xCorrelationId,
			@Header("Authorization") String auth
			);
	
}
