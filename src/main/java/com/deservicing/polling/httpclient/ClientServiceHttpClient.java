package com.deservicing.polling.httpclient;

import com.deservicing.polling.model.dto.GenericResponse;
import com.deservicing.polling.model.dto.clientservice.AddressChangeRequestDto;

import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Header;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.client.annotation.Client;

@Client("${httpclient.clientService}")
public interface ClientServiceHttpClient {

	@Post("/address")
	GenericResponse<Object> addressChange(
			@Body AddressChangeRequestDto request,
			@Header("x-traceability-id") String xTraceAbilityId,
			@Header("x-correlation-id") String xCorrelationId,
			@Header("Authorization") String auth
			);
}
