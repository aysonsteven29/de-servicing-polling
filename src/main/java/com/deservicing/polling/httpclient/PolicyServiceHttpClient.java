package com.deservicing.polling.httpclient;

import com.deservicing.polling.model.dto.GenericResponse;
import com.deservicing.polling.model.dto.policyservice.PolicyResponseDto;

import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Header;
import io.micronaut.http.annotation.QueryValue;
import io.micronaut.http.client.annotation.Client;

@Client("${httpclient.policyService}")
public interface PolicyServiceHttpClient {

	@Get("/policies")
	GenericResponse<PolicyResponseDto> getStaticInfoPolicies(
			@QueryValue("optimized") Boolean optimized,
			@Header("x-traceability-id") String xTraceAbilityId,
			@Header("x-correlation-id") String xCorrelationId,
			@Header("Authorization") String auth,
			@Header("policyNumbers") String plicyNumbers
			);
}
