## Micronaut 3.6.3 Documentation

- [User Guide](https://docs.micronaut.io/3.6.3/guide/index.html)
- [API Reference](https://docs.micronaut.io/3.6.3/api/index.html)
- [Configuration Reference](https://docs.micronaut.io/3.6.3/guide/configurationreference.html)
- [Micronaut Guides](https://guides.micronaut.io/index.html)
---

## Feature test-resources documentation

- [Micronaut Test Resources documentation](https://micronaut-projects.github.io/micronaut-test-resources/latest/guide/)


## Feature jdbc-hikari documentation

- [Micronaut Hikari JDBC Connection Pool documentation](https://micronaut-projects.github.io/micronaut-sql/latest/guide/index.html#jdbc)


